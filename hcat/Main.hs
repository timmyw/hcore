module Main where

import System.Environment

main :: IO ()
main = do
  args <- getArgs
  mapM_ doCat args

doCat file =
  do
    contents <- readFile file
    let lines_ = lines contents
    mapM_ putStrLn lines_
