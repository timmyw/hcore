# hcore

Haskell implementations of various core-utils

Used as a learning tool, not necessarily as day to day tools.

## Utils

### hcat

`cat` replacement.
Cats the files specified as arguments to stdout
